const mongoose = require("mongoose");

const roomSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  user_id: String,
  name: String,
  type: String,
  capacity: Number
});

module.exports = mongoose.model("Room", roomSchema);
