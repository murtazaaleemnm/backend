const mongoose = require("mongoose");

const rulesSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  user_id: String,
  class_hour_limit_per_day: Number,
  work_hour_limit_per_day: Number,
  work_hour_teacher_limit_per_day: Number,
});

module.exports = mongoose.model("Rules", rulesSchema);
