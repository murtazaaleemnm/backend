const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const Room = require("../models/room");
const _ = require("lodash");

//GET request huleen avah
router.get("/:userId", (req, res, next) => {
  Room.find()
    .exec()
    .then((docs) => {
      console.log(docs);
      if (docs.length >= 0) {
        let grouped = _.groupBy(docs, "user_id");
        res.status(200).json(grouped[req.params.userId]);
      } else {
        res.status(404).json({
          message: "No entries found",
        });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    });
});
//POST request huleen avah
router.post("/:userId", (req, res, next) => {
  //doorh n shine object uusgeed tuund utguudiig onoon yvuulna
  const room = new Room({
    _id: mongoose.Types.ObjectId(),
    user_id: req.params.userId,
    name: req.body.roomNumber,
    type: req.body.type,
    capacity: req.body.capacity,
  });
  room
    .save()
    .then((result) => {
      console.log(result);
    })
    .catch((err) => console.log(err));
  res.status(201).json({
    message: "Handling POST request to /rooms",
    createdRoom: room,
  });
});

router.get("/:userId/:roomId", (req, res, next) => {
  const id = req.params.roomId;
  Room.findById(id)
    .exec()
    .then((doc) => {
      console.log(doc);
      if (doc) {
        res.status(200).json(doc);
      } else {
        res.status(404).json({
          message: "no valid id found",
        });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err });
    });
});

router.patch("/:roomId", (req, res, next) => {
  const id = req.params.roomId;
  const updateOps = {};
  //request yvyylahda [{"propName":"name", "value":"new value"}] gesen zagvaraar yvuulah
  //doorh for functs tuslamjtaigaar umar ch turliin update avah bolomjtoi bolno
  for (const ops of req.body) {
    updateOps[ops.propName] = ops.value;
  }
  //update functs n uurchluh gj bga object iin id-g avaad uurchluh field iin avna
  Room.update({ _id: id }, { $set: updateOps })
    .exec()
    .then((result) => {
      console.log(result);
      res.status(200).json(result);
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err });
    });
});

router.delete("/:roomId", (req, res, next) => {
  const id = req.params.roomId;
  Room.remove({ _id: id })
    .exec()
    .then((result) => {
      res.status(200).json(result);
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err });
    });
});

module.exports = router;
