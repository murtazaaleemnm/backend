/**
 * Goal: create week schedule based on given data and rules
 * Instruction Step1: see main heart section (post request)
 * Instruction Step2: see core function section (all functions that begin with return)
 */

const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const Time = require("../models/time");
const Par = require("../models/par");
const Activity = require("../models/activity");
const Teacher = require("../models/teacher");
const Class = require("../models/class");
const Room = require("../models/room");
const Lesson = require("../models/lesson");
const Rules = require("../models/rules");
const _ = require("lodash");

/**=====================================================================================================================================================
 * core functions that are used to create week schedule starts
 * =====================================================================================================================================================
 */
//zaah heregtei tsagaa oloogui uldsen hicheeluudiin jagsaalt
let remainingLessons = null;

const returnWeekTime = (teachers, rules, rooms) => {
  let time = [];
  for (let i = 0; i < 7; i++) {
    let day = null;
    // let you create empty rooms timetable
    let rooms_day = rooms.map((room) => {
      let room_day = null;
      let pars = [];
      for (let i = 0; i < rules.work_hour_limit_per_day; i++) {
        let par = {
          room_booked: false,
          lesson: null,
        };
        pars.push(par);
      }
      room_day = {
        room: room,
        pars: pars,
      };
      return room_day;
    });
    // let you create empty teachers timetable
    let teachers_day = teachers.map((teacher) => {
      let teacher_day = null;
      let pars = [];
      // we use work_hour_limit_per_day for teacher in order to have empty pars during day
      for (let j = 0; j < rules.work_hour_limit_per_day; j++) {
        let par = {
          id: j,
          classs: null,
          room: null,
          lesson: null,
        };
        pars.push(par);
      }
      teacher_day = {
        teacher: teacher,
        pars: pars,
      };
      return teacher_day;
    });
    day = {
      id: i,
      teachers: teachers_day,
      rooms: rooms_day,
    };
    time.push(day);
  }
  return time;
};

//returns all lessons that needs to be teached
const returnLessons = (classes) => {
  let allLessons = [];
  let count = 0;
  classes.map((classs) => {
    let must_lessons = classs.must_lessons;
    let room_number = classs.room_number;
    let teacher_code = classs.teacher_code;

    must_lessons.map((must_lesson, index) => {
      for (i = 0; i < must_lesson.time; i++) {
        let lesson = {
          _id: count,
          class_name: classs.name,
          lesson: must_lesson.id,
          room: room_number,
          teacher: teacher_code,
        };
        count = count + 1;
        allLessons.push(lesson);
      }
    });
  });
  return allLessons;
};

//function for filling and getting timetable
const returnWeekTimeWithLessons = (
  time,
  allLessons,
  remainingLessons,
  rules
) => {
  for (i = 0; i < allLessons.length; i++) {
    time = addToTime(time, allLessons[i], remainingLessons, rules);
  }
  return time;
};

//uncomment for high school
//hoerdugaar eeljnii duurgelt
// const returnWeekTimeWithLessons2 = (time, allLessons, remainingLessons) => {
//   for (i = 0; i < allLessons.length; i++) {
//     time = addToTime2(time, allLessons[i], remainingLessons);
//   }
//   return time;
// };

//timetable filling
const addToTime = (time, lesson, remainingLessons, rules) => {
  let count = 0;
  let test_teacher = null;

  //the loop is 7 becuase the week has 7 day
  //[can be dynamic we need rules page to get it from user]
  for (let i = 0; i < 7; i++) {
    let day = time[i];
    // for (let k = 0; k < day.teachers[0].pars.length; k++) {
    for (let k = 0; k < rules.work_hour_limit_per_day; k++) {
      for (let j = 0; j < day.teachers.length; j++) {
        let teacher = day.teachers[j];
        test_teacher = teacher;
        if (teacher.pars[k].classs === null) {
          if (
            checkIfRemaining(
              teacher.teacher.lessons,
              lesson,
              day,
              k,
              remainingLessons,
              day.rooms,
              j,
              rules
            )
          ) {
            time[i].teachers[j].pars[k].classs = lesson.class_name;
            time[i].teachers[j].pars[k].room = lesson.room;
            time[i].teachers[j].pars[k].lesson = lesson.lesson;
            for (let m = 0; m < day.rooms.length; m++) {
              if (lesson.room === day.rooms[m].room.name) {
                day.rooms[m].pars[k].room_booked = true;
                day.rooms[m].pars[k].lesson = lesson;
              }
            }
            count++;
          }
        }
      }
    }
  }

  return time;
};

//uncomment for high school
//hoerdugaar eeljnii tsag duurgelt

// const addToTime2 = (time, lesson, remainingLessons) => {
//   let count = 0;
//   let test_teacher = null;

//   for (let i = 0; i < 7; i++) {
//     let day = time[i];
//     console.log("-------------->", day);
//     // for (let k = 0; k < day.teachers[0].pars.length; k++) {
//     for (let k = 7; k < 13; k++) {
//       for (let j = 0; j < day.teachers.length; j++) {
//         let teacher = day.teachers[j];
//         test_teacher = teacher;
//         if (teacher.pars[k].classs === null) {
//           if (
//             checkIfRemaining(
//               teacher.teacher.lessons,
//               lesson,
//               day,
//               k,
//               remainingLessons
//             )
//           ) {
//             time[i].teachers[j].pars[k].classs = lesson.class_name;
//             time[i].teachers[j].pars[k].room = lesson.room;
//             time[i].teachers[j].pars[k].lesson = lesson.lesson;
//             count++;
//           }
//         }
//       }
//     }
//   }

//   return time;
// };
//Tuhain nudend tuhain hicheeliig tahiv bolomjtoi esgiig shalgana
//Check if it is possible to place that lesson into the cell
const checkIfRemaining = (
  teaching_lessons,
  lesson,
  day,
  par_id,
  remainingLessons,
  rooms,
  teacher_id,
  rules
) => {
  let index = null;
  let res = false;
  let room_name_to_book = lesson.room;
  let count = 0;

  for (let i = 0; i < day.teachers[teacher_id].pars.length; i++) {
    if (day.teachers[teacher_id].pars[i].lesson !== null) {
      count++;
    }
  }
  if (count >= rules.work_hour_teacher_limit_per_day) {
    return false;
  }

  for (let i = 0; i < rooms.length; i++) {
    if (room_name_to_book === rooms[i].room.name) {
      if (rooms[i].pars[par_id].room_booked !== false) {
        return false;
      }
    }
  }

  for (let i = 0; i < remainingLessons.length; i++) {
    if (lesson._id === remainingLessons[i]._id) {
      index = i;
    }
  }
  for (let i = 0; i < teaching_lessons.length; i++) {
    if (teaching_lessons[i].value._id === lesson.lesson) {
      res = true;
    }
  }
  for (let i = 0; i < day.teachers.length; i++) {
    if (day.teachers[i].pars[par_id] !== null) {
      if (day.teachers[i].pars[par_id].classs === lesson.class_name) {
        res = false;
      }
    }
  }

  if (index !== null && res === true) {
    remainingLessons.splice(index, 1);
    return true;
  } else {
    return false;
  }
};
/**=====================================================================================================================================================
 * core functions that are used to create week schedule ends
 * =====================================================================================================================================================
 */

/**======================================================================================================================================================
 * main heart of the scheduling algo start
 * =====================================================================================================================================================
 */
//POST request huleen avah
router.post("/:userId", async (req, res, next) => {
  console.log("[create]");
  let teachers = null;
  let rooms = null;
  let classes = null;
  let lessons = null;
  let week = null;
  let weeks = null;
  let roomsClasses = null;
  let rules = null;
  //gets week schedule if it is not empty deletes previos schedule

  weeks = await Time.find()
    .exec()
    .then((docs) => {
      console.log(docs);
      if (docs.length >= 0) {
        let grouped = _.groupBy(docs, "user_id");
        return grouped[req.params.userId];
      } else {
        return null;
      }
    })
    .catch((err) => {
      console.log("[create error]:", err);
      return err;
    });
  if (weeks && weeks !== null) {
    for (i = 0; i < weeks.length; i++) {
      Time.remove({ _id: weeks[i]._id })
        .exec()
        .then((result) => {
          console.log("[cleared week]");
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }

  //gets all teachers info from DB

  teachers = await Teacher.find()
    .exec()
    .then((docs) => {
      if (docs.length >= 0) {
        let grouped = _.groupBy(docs, "user_id");
        return grouped[req.params.userId];
      }
    })
    .catch((err) => {
      console.log("[create error]:", err);
      return err;
    });

  //gets all rooms info from DB
  rooms = await Room.find()
    .exec()
    .then((docs) => {
      if (docs.length >= 0) {
        let grouped = _.groupBy(docs, "user_id");
        return grouped[req.params.userId];
      } else {
        return {
          message: "No entries found",
        };
      }
    })
    .catch((err) => {
      console.log(err);
      return err;
    });

  //gets all lessons info from DB

  lessons = await Lesson.find()
    .exec()
    .then((docs) => {
      if (docs.length >= 0) {
        let grouped = _.groupBy(docs, "user_id");
        return grouped[req.params.userId];
      } else {
        return {
          message: "No entries found",
        };
      }
    })
    .catch((err) => {
      console.log(err);
      return err;
    });

  //gets all classes info from DB

  classes = await Class.find()
    .exec()
    .then((docs) => {
      if (docs.length >= 0) {
        let grouped = _.groupBy(docs, "user_id");
        return grouped[req.params.userId];
      } else {
        return {
          message: "No entries found",
        };
      }
    })
    .catch((err) => {
      console.log(err);
      return err;
    });
  //gets all rules info from DB

  rules = await Rules.find()
    .exec()
    .then((docs) => {
      if (docs.length >= 0) {
        let grouped = _.groupBy(docs, "user_id");
        return grouped[req.params.userId];
      } else {
        return {
          message: "No entries found",
        };
      }
    })
    .catch((err) => {
      console.log(err);
      return err;
    });
  // const teachers_time = await returnTeachersTime(teachers);

  // class_hour_limit_per_day
  // work_hour_limit_per_day
  // work_hour_teacher_limit_per_day
  let time = await returnWeekTime(teachers, rules[0], rooms); // will return empty 7 day time schedule of teacher
  let allLessons = await returnLessons(classes); //will return all lessons that needs to be teached
  remainingLessons = [...allLessons]; // makes copy of all lessons to use later to check if all lessons are placed in time schedule
  let new_time = await returnWeekTimeWithLessons(
    time,
    allLessons,
    remainingLessons,
    rules[0]
  ); // puts lessons into time schedule

  //uncomment for high school
  // if (remainingLessons.length > 0) {
  //   new_time = await returnWeekTimeWithLessons2(
  //     new_time,
  //     allLessons,
  //     remainingLessons
  //   ); // puts lessons into time schedule if there is more lessons to place
  // }

  // creates week
  week = new Time({
    _id: mongoose.Types.ObjectId(),
    user_id: req.params.userId,
    par_count: req.body.par_count,
    week: new_time,
  });

  week
    .save()
    .then((result) => {
      console.log(result);
    })
    .catch((err) => console.log(err));
  res.status(201).json({
    message: "Handling POST request to /time",
    createdTime: time,
  });
});
/**=====================================================================================================================================================
 * main heart of the scheduling algo end
 * =====================================================================================================================================================
 */

//GET request huleen avah no time id
router.get("/:userId", (req, res, next) => {
  Time.find()
    .exec()
    .then((docs) => {
      console.log(docs);
      if (docs.length >= 0) {
        let grouped = _.groupBy(docs, "user_id");
        res.status(200).json(grouped[req.params.userId]);
      } else {
        res.status(404).json({
          message: "No entries found",
        });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    });
});

//GET request huleen avah with time id
router.get("/:userId/:timeId", (req, res, next) => {
  const id = req.params.timeId;
  Time.findById(id)
    .exec()
    .then((doc) => {
      console.log(doc);
      if (doc) {
        res.status(200).json(doc);
      } else {
        res.status(404).json({
          message: "no valid id found",
        });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err });
    });
});

router.patch("/:timeId", (req, res, next) => {
  const id = req.params.timeId;
  const updateOps = {};
  //request yvyylahda [{"propName":"name", "value":"new value"}] gesen zagvaraar yvuulah
  //doorh for functs tuslamjtaigaar umar ch turliin update avah bolomjtoi bolno
  for (const ops of req.body) {
    updateOps[ops.propName] = ops.value;
  }
  //update functs n uurchluh gj bga object iin id-g avaad uurchluh field iin avna
  Time.update({ _id: id }, { $set: updateOps })
    .exec()
    .then((result) => {
      console.log(result);
      res.status(200).json(result);
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err });
    });
});

router.delete("/:timeId", (req, res, next) => {
  const id = req.params.timeId;
  Time.remove({ _id: id })
    .exec()
    .then((result) => {
      res.status(200).json(result);
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err });
    });
});

module.exports = router;
