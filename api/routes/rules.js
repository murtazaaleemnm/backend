const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const Rules = require("../models/rules");
const _ = require("lodash");

//GET request huleen avah
router.get("/:userId", (req, res, next) => {
  Rules.find()
    .exec()
    .then((docs) => {
      console.log(docs);
      if (docs.length >= 0) {
        let grouped = _.groupBy(docs, "user_id");
        res.status(200).json(grouped[req.params.userId]);
      } else {
        res.status(404).json({
          message: "No entries found",
        });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    });
});
//POST request huleen avah
router.post("/:userId", (req, res, next) => {
  //doorh n shine object uusgeed tuund utguudiig onoon yvuulna
  const rule = new Rules({
    _id: mongoose.Types.ObjectId(),
    user_id: req.params.userId,
    class_hour_limit_per_day: req.body.class_hour_limit_per_day,
    work_hour_limit_per_day: req.body.work_hour_limit_per_day,
    work_hour_teacher_limit_per_day: req.body.work_hour_teacher_limit_per_day,
  });
  rule
    .save()
    .then((result) => {
      console.log(result);
    })
    .catch((err) => console.log(err));
  res.status(201).json({
    message: "Handling POST request to /rules",
    createdRule: rule,
  });
});

router.get("/:userId/:ruleId", (req, res, next) => {
  const id = req.params.ruleId;
  Rules.findById(id)
    .exec()
    .then((doc) => {
      console.log(doc);
      if (doc) {
        res.status(200).json(doc);
      } else {
        res.status(404).json({
          message: "no valid id found",
        });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err });
    });
});

router.patch("/:ruleId", (req, res, next) => {
  const id = req.params.ruleId;
  const { ...data } = req.body;
  //update functs n uurchluh gj bga object iin id-g avaad uurchluh field iin avna
  Rules.update({ _id: id }, { $set: data })
    .exec()
    .then((result) => {
      console.log(result);
      res.status(200).json(result);
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err });
    });
});

router.delete("/:ruleId", (req, res, next) => {
  const id = req.params.ruleId;
  Rules.remove({ _id: id })
    .exec()
    .then((result) => {
      res.status(200).json(result);
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err });
    });
});

module.exports = router;
