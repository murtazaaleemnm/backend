const express = require("express");
const router = express.Router();
const User = require("../models/user");

//login oos ireh huseltiig barij avaad terhuu hereglegch bga tohioldold hereglegchiin id-g butsaah bolno
router.post("/", (req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;
  let id = null;
  console.log("[request]", req.body);
  User.find()
    .exec()
    .then((docs) => {
      console.log(docs);
      if (docs.length >= 0) {
        for (i = 0; i < docs.length; i++) {
          if (docs[i].username === username && docs[i].password === password) {
            id = docs[i]._id;
          }
        }
        if (id) {
          res.status(200).json({ id: id });
        } else {
          res.status(400).json({
            error: "username or password incorrect",
          });
        }
      } else {
        res.status(404).json({
          message: "no data",
        });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        error: err,
      });
    });
});

module.exports = router;
